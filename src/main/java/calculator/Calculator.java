package calculator;

public class Calculator {

	double mul(double x, double y) {
		return y * x;
	}
	
	double div(double x, double y) {
		return x / y;
	}
	
	double add(double x, double y) {
		return x + y;
	}
	
	double sub(double x, double y) {
		return x - y;
	}
	
	double addSub(double x, double y, double z) {
		return add(sub(x, z), y);
	}
	
	double subAdd(double x, double y, double z) {
		return sub(add(x, z), y);
	}
}
